import axios from 'axios'

export function request(config){
  //1.创建axios实例
  const instance = axios.create({
    baseURL:'http://123.207.32.32:8000',
    timeout:5000
  })

  //2.拦截器
  instance.interceptors.request.use(res=>{
    console.log(res)
    //(1)config中的一些信息不符合服务器要求
    //(2)每次发送网络请求时，显示一个请求的图标
    //(3)某些请求必须携带特殊信息(token)
    return res
  },err=>{
    console.log(err)
  })

  instance.interceptors.response.use(res=>{
    console.log(res)
    return res.data
  },err=>{
    console.log(err)
  })

  //3.发送真正的请求
  return instance(config)
}
